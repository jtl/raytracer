//
//  CornellBox.h
//  Raytracer
//
//  Created by James Loomis on 11/9/12.
//  Copyright (c) 2012 James Loomis. All rights reserved.
//

#ifndef __Raytracer__CornellBox__
#define __Raytracer__CornellBox__

#include <iostream>
#include <vector>
#include "Polygon.h"

void add_cornell_box_walls(vector<Shape *> &world);
void add_cornell_tallbox(vector<Shape *> &world);
void add_cornell_shortbox(vector<Shape *> &world);

#endif /* defined(__Raytracer__CornellBox__) */
