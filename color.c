////
//// FILE: color.cpp
////
//// class for holding a color value.
//// contains various functions / overloaded operators for
//// performing operations on colors.
////
////

#include "setPixel.h"
#include "color.h"

color currentcolor;
color clearcolor;

color::color(float _r,float _g,float _b) : r(_r),g(_g),b(_b) {
}

color::color(double *a) : r(a[0]),g(a[1]),b(a[2]) {
}

color::color() : r(0.0),g(0.0),b(0.0) {
}

void color::set(float _r,float _g,float _b) {
	r = _r;
	g = _g;
	b = _b;
}

void color::max() {
	if(r > 1.0) r = 1.0;
	if(g > 1.0) g = 1.0;
	if(b > 1.0) b = 1.0;
}

double color::luminance() {
	// For checkpoint 7
	return 0.27 * r + 0.67 * g + 0.06 * b;
}

color  color::operator* (const color& c) {
	color result;
	result.r = r * c.r;
	result.g = g * c.g;
	result.b = b * c.b;
	return result;
}

color  color::operator+ (const float& f) {
	color result;
	result.r = r + f;
	result.g = g + f;
	result.b = b + f;
	return result;
}

color  color::operator- (const color& c) {
	color result;
	result.r = r - c.r;
	result.g = g - c.g;
	result.b = b - c.b;
	return result;
}

color  color::operator/ (const color& c) {
	color result;
	result.r = r / c.r;
	result.g = g / c.g;
	result.b = b / c.b;
	return result;
}
color  color::operator/ (const float& f) {
	color result;
	result.r = r/f; 
	result.g = g/f;
	result.b = b/f;
	return result;
}

color  color::operator* (const float& f) {
	color result;
	result.r = r*f; 
	result.g = g*f;
	result.b = b*f;
	return result;
}

color& color::operator=(const color& c) {
	r=c.r;
	g=c.g;
	b=c.b;
	return *this;
}

color& color::operator+=(const color& c) {
	r+=c.r;
	g+=c.g;
	b+=c.b;
	return *this;
}

color& color::operator*=(const float& f) {
	r*=f;
	g*=f;
	b*=f;
	return *this;
}

color& color::operator/=(const float& f) {
	r/=f;
	g/=f;
	b/=f;
	return *this;
}
ostream& operator<<(ostream& os, const color& c) {
	os << "(" << c.r << ", " << c.g << ", " << c.b << ")";
	return os;
}

/**
 * 2 argument setPixel allows me to use
 * code from assignment 2 without changing
 * setPixel calls
 */
void setPixel(int x,int y,color c) {
	setPixel(x,y,c.r,c.g,c.b);
}

/**
 * return current drawing color
 */
color getcolor() {
	return currentcolor;
}

/**
 * return current clear color
 */
color getclearcolor() {
	return clearcolor;
}

/**
 * set current drawing color
 */
void setcolor(float r,float g,float b) {
	currentcolor.r=r;
	currentcolor.g=g;
	currentcolor.b=b;
}

/**
 * set current clear color
 */
void setclearcolor(float r,float g,float b) {
	clearcolor.r=r;
	clearcolor.g=g;
	clearcolor.b=b;
}

