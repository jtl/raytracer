//
//  CornellBox.cpp
//  Raytracer
//
//  Created by James Loomis on 11/9/12.
//  Copyright (c) 2012 James Loomis. All rights reserved.
//

#include "CornellBox.h"



// Add short box from Basic Cornell Box
void add_cornell_shortbox(vector<Shape *> &world) {
    
    
    //Object shortBox
   
     //# Top Face
    Polygon *topface1 = new Polygon(
    0.53, 0.6, 0.75,
    0.7, 0.6, 0.17,
    0.13, 0.6, 0,
    -0.05, 0.6, 0.57);
     topface1->setAmbient(0.725, 0.71, 0.68);
     topface1->setDiffuse(0.725, 0.71, 0.68);
     topface1->setSpecular(0.0, 0.0, 0.0);
   
    //topface
    Polygon *leftface1 = new Polygon(
    -0.05, 0, 0.57,
    -0.05, 0.6, 0.57,
    0.13, 0.6, 0,
    0.13, 0, 0);
    leftface1->setAmbient(0.725, 0.71, 0.68);
    leftface1->setDiffuse(0.725, 0.71, 0.68);
    leftface1->setSpecular(0.0, 0.0, 0.0);
    
    //# Front Face
    Polygon *frontface1= new Polygon(
    0.53, 0, 0.75,
    0.53, 0.6, 0.75,
    0.05 ,0.6, 0.57,
    0.05, 0, 0.57);
    frontface1->setAmbient(0.725, 0.71, 0.68);
    frontface1->setDiffuse(0.725, 0.71, 0.68);
    frontface1->setSpecular(0.0, 0.0, 0.0);
    
    //# Right Face
    Polygon *rightface1 = new Polygon(
    0.7, 0, 0.17,
    0.7, 0.6, 0.17,
    0.53, 0.6, 0.75,
    0.53, 0, 0.75);
    rightface1->setAmbient(0.725, 0.71, 0.68);
    rightface1->setDiffuse(0.725, 0.71, 0.68);
    rightface1->setSpecular(0.0, 0.0, 0.0);

    //# Back Face
    Polygon *backface1= new Polygon(
    0.13, 0, 0,
    0.13, 0.6, 0,
    0.7, 0.6, 0.17,
    0.7, 0, 0.17);
    backface1->setAmbient(0.725, 0.71, 0.68);
    backface1->setDiffuse(0.725, 0.71, 0.68);
    backface1->setSpecular(0.0, 0.0, 0.0);
    

    //# Bottom Face
    Polygon *bottomface1= new Polygon(
    0.53, 0, 0.75,
    0.7, 0, 0.17,
    0.13, 0, 0,
    -0.05, 0, 0.57);
    bottomface1->setAmbient(0.725, 0.71, 0.68);
    bottomface1->setDiffuse(0.725, 0.71, 0.68);
    bottomface1->setSpecular(0.0, 0.0, 0.0);
    
    world.push_back(backface1);
    world.push_back(frontface1);
    world.push_back(topface1);
    world.push_back(bottomface1);
    world.push_back(leftface1);
    world.push_back(rightface1);
   
}

// Add Basic Cornell Box
void add_cornell_tallbox(vector<Shape *> &world) {

     //# Top Face
    Polygon *topface= new Polygon(
    -0.53,1.2,0.09,
    0.04,1.2,-0.09,
    -0.14,1.2,-0.67,
    -0.71,1.2,-0.49);
    topface->setAmbient(0.725, 0.71, 0.68);
    topface->setDiffuse(	0.725, 0.71, 0.68);
    topface->setSpecular(0.0, 0.0, 0.0);

     Polygon *leftface= new Polygon(
    -0.53, 0.0, 0.09,
    -0.53, 1.2, 0.09,
    -0.71, 1.2, -0.49,
    -0.71, 0.0, -0.49);
    leftface->setAmbient(0.725, 0.71, 0.68);
    leftface->setDiffuse(	0.725, 0.71, 0.68);
    leftface->setSpecular(0.0, 0.0, 0.0);
                    

    //# Back Face
     Polygon *backface= new Polygon(
    -0.71, 0.0, -0.49,
    -0.71, 1.2, -0.49,
    -0.14, 1.2, -0.67,
    -0.14, 0.0, -0.67);
    backface->setAmbient(0.725, 0.71, 0.68);
    backface->setDiffuse(	0.725, 0.71, 0.68);
    backface->setSpecular(0.0, 0.0, 0.0);
    

    //# Right Face
     Polygon *rightface= new Polygon(
    -0.14, 0.0, -0.67,
    -0.14, 1.2, -0.67,
    0.04, 1.2, -0.09,
    0.04, 0, -0.09);
    rightface->setAmbient(0.725, 0.71, 0.68);
    rightface->setDiffuse(	0.725, 0.71, 0.68);
    rightface->setSpecular(0.0, 0.0, 0.0);

    //# Front Face
    Polygon *frontface= new Polygon(
    0.04, 0, -0.09,
    0.04, 1.2, -0.09,
    -0.53, 1.2, 0.09,
    -0.53, 0, 0.09);
    frontface->setAmbient(0.725, 0.71, 0.68);
    frontface->setDiffuse(	0.725, 0.71, 0.68);
    frontface->setSpecular(0.0, 0.0, 0.0);

    //# Bottom Face
     Polygon *bottomface= new Polygon(
    -0.53, 0, 0.09,
    0.04, 0, -0.09,
    -0.14, 0, -0.67,
    -0.71, 0, -0.49);
    bottomface->setAmbient(0.725, 0.71, 0.68);
    bottomface->setDiffuse(	0.725, 0.71, 0.68);
    bottomface->setSpecular(0.0, 0.0, 0.0);
    
    world.push_back(backface);
    world.push_back(frontface);
    world.push_back(topface);
    world.push_back(bottomface);
    world.push_back(leftface);
    world.push_back(rightface);

}

// Add Basic Cornell Box
void add_cornell_box_walls(vector<Shape *> &world) {
    
    
    //# Object floor
    Polygon *floor = new Polygon(-1.01,0.0,0.99,
                  1.0,0.0,0.99,
                  1.0, 0.0, -1.04,
                  -0.99, 0.0, -1.04);
    floor->setAmbient(0.725, 0.71, 0.68);
    floor->setDiffuse(0.725, 0.71, 0.68);
    floor->setSpecular(0.0, 0.0, 0.0);

    //# Object ceiling
    Polygon *ceiling= new Polygon(
    -1.02, 1.99, 0.99 ,
    -1.02, 1.99, -1.04,
    1.0,   1.99, -1.04,
    1.0,   1.99, 0.99 );
    ceiling->setAmbient(0.725, 0.71, 0.68);
    ceiling->setDiffuse(	0.725, 0.71, 0.68);
    ceiling->setSpecular(0.0, 0.0, 0.0);

    //# Object backwall
     Polygon *backwall= new Polygon(
    -0.99,0,-1.04,
    1,0,-1.04,
    1,1.99,-1.04,
    -1.02,1.99,-1.04);
    backwall->setAmbient(0.725, 0.71, 0.68);
    backwall->setDiffuse(	0.725, 0.71, 0.68);
    backwall->setSpecular(0.0, 0.0, 0.0);

    //## Object rightwall
     Polygon *rightwall= new Polygon(
    1,0,-1.04,
    1,0,0.99,
    1,1.99,0.99,
    1,1.99,-1.04);
    rightwall->setAmbient(0.14, 0.45, 0.091);
    rightwall->setDiffuse(0.14, 0.45, 0.091);
    rightwall->setSpecular(0.0, 0.0, 0.0);
    
    //## Object leftWall
     Polygon *leftwall= new Polygon(
    -1.01,0,0.99,
    -0.99,0,-1.04,
    -1.02,1.99,-1.04,
    -1.02,1.99,0.99);
    leftwall->setAmbient(0.63, 0.065, 0.05);
    leftwall->setDiffuse(0.63, 0.065, 0.05);
    leftwall->setSpecular(0.0, 0.0, 0.0);
    

    
    //## Object light
     Polygon *light= new Polygon(
    -0.24,1.98,0.16,
    -0.24,1.98,-0.22,
    0.23,1.98,-0.22,
    0.23,1.98,0.16);
    light->setAmbient(0.78, 0.78, 0.78);
    light->setDiffuse(0.78, 0.78, 0.78);
    light->setSpecular(0.0, 0.0, 0.0);
    
    world.push_back(backwall);
    world.push_back(floor);
    world.push_back(ceiling);
    world.push_back(leftwall);
    world.push_back(rightwall);
    
}
