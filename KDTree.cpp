//
//  KDTree.cpp
//  Raytracer
//
//  Created by James Loomis on 9/25/12.
//  Copyright (c) 2012 James Loomis. All rights reserved.
//

#include "KDTree.h"

#include <cassert>

KDTree::KDTree(vector<Shape *> objects) : world(objects),isleaf(false), left(0), right(0) {
    //  Node getNode (List of primitives L, Voxel V) {
    //      if (Terminate (L, V)) return new leaf node (L)
    //
    //      Find partition plane P
    //      Split V with P producing VFRONT and VREAR
    //      Partition elements of L producing LFRONT and LREAR
    //      return new interior node (P,getNode(LFRONT, VFRONT),getNode(LREAR, VREAR))
    //  }
    
    // Should I need to check this?
    if(world.size() == 0)
        return;
    
    voxel = world.at(0)->getAABB();
    for(int shape=1;shape<world.size();shape++) {
        voxel += world.at(shape)->getAABB();
    }
    
    getNode(0);
}

KDTree::KDTree(vector<Shape *> objects,AABB bb,int depth) : world(objects),isleaf(false),
    voxel(bb),left(0),right(0) {
    getNode(depth);
}

int KDTree::size() {
    if(isleaf) {
        return world.size();
    } else {
        assert( left > 0 && right > 0 );
        return left->size() + right->size();
    }
}

Shape *KDTree::firstIntersection(Ray &r) {
    if(! voxel.intersects(r,0.0,10000.0)) {
        // Doesn't intersect the AABB of this node at all!
        return NULL;
    }
    if(isleaf) {
        // check each node! (rip from raytrace.cpp?)
        Shape *hit = NULL;
        float dist = 10000.0;
        
        for(int shape=0;shape<world.size();shape++) {
            
            Ray rshape(r);
            
            Shape *s = world.at(shape);
            int intersected = s->trace(rshape);
            
            if(intersected > 0) {
                double d = pt3_distance(rshape.P,rshape.intersection);
                if(d < dist) {
                    dist = d;
                    hit = s;
                }
            }
        }
        
        return hit;
        
    }
    
    float left_dist;
    float right_dist;
    
    bool intersects_left = left->voxel.intersect2(r,left_dist);
    bool intersects_right = right->voxel.intersect2(r,right_dist);
    
    Shape *hit = NULL;
    
    if(intersects_left && intersects_right) {
        if(left_dist < right_dist) {
            hit = left->firstIntersection(r);
            if(hit == NULL) {
                hit = right->firstIntersection(r);
            }
        } else {
            hit = right->firstIntersection(r);
            if(hit == NULL) {
                hit = left->firstIntersection(r);
            }
        }
    } else if(intersects_left) {
        hit = left->firstIntersection(r);
    } else if(intersects_right) {
        hit = right->firstIntersection(r);
    }

    return hit;
}

void KDTree::getNode(int depth) {
    int MAX_SIZE = 10;
    int MAX_DEPTH = 25;
    if(world.size() <= MAX_SIZE || depth == MAX_DEPTH) {
        // No need to subdivide further, already has MAX_SIZE or fewer objects
        isleaf = true;
        //printf("Leaf node created: %ld objects, depth of %d.\n", world.size(),depth);
    } else {
        isleaf = false;
        
        // subdivide further
        AABB leftVoxel;
        AABB rightVoxel;
        vector<Shape *> leftShapes;
        vector<Shape *> rightShapes;
        sepaxis = depth % 3;
        
        sepplane = voxel.split(sepaxis,leftVoxel,rightVoxel);
        
        for(int shape=0;shape<world.size();shape++) {
            if(leftVoxel.intersects(world.at(shape)->getAABB())) {
                //add to left child
                leftShapes.push_back(world.at(shape));
            }
            if(rightVoxel.intersects(world.at(shape)->getAABB())) {
                //add to right child
                rightShapes.push_back(world.at(shape));
            }
        }

	world.clear();
        
        left = new KDTree(leftShapes,leftVoxel,depth+1);
        right = new KDTree(rightShapes,rightVoxel,depth+1);

    }

}

KDTree::~KDTree() {
    if(left > 0)
        delete left;
    
    if(right > 0) 
        delete right;
}
