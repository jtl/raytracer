
#include "Shape.h"
#include "Triangle.h"
#include <assert.h>

#ifndef __POLYGON_H

#define __POLYGON_H

class Polygon : public Shape {
	public:
		Polygon(pt3 a,pt3 b,pt3 c,pt3 d);
        Polygon(float,float,float,
                float,float,float,
                float,float,float,
                float,float,float);
		int trace(Ray &r);
        void split(vector<Shape *> & objects);
        AABB getAABB();
	private:
    Triangle *t1;
    Triangle *t2;
};

#endif // __POLYGON_H