//
//  Triangle.cpp
//  Raytracer
//

#include "Triangle.h"

Triangle::Triangle(pt3 _p0,pt3 _p1,pt3 _p2) {
	pt3_copy(this->p0,_p0);
	pt3_copy(this->p1,_p1);
	pt3_copy(this->p2,_p2);
	print();
}

Triangle::Triangle(float p0x,float p0y,float p0z,
		   float p1x,float p1y,float p1z,
		   float p2x,float p2y,float p2z) {

	pt3_set(this->p0,p0x,p0y,p0z);
	pt3_set(this->p1,p1x,p1y,p1z);
	pt3_set(this->p2,p2x,p2y,p2z);
	print();
}

void Triangle::print() {
	/*
	static pt3 max,min;
	printf("Triangle at: \n");
	printf("\t(%4.2f %4.2f %4.2f)\n",p0[0],p0[1],p0[2]);
	printf("\t(%4.2f %4.2f %4.2f)\n",p1[0],p1[1],p1[2]);
	printf("\t(%4.2f %4.2f %4.2f)\n",p2[0],p2[1],p2[2]);
	for(int i=0;i<3;i++) {
		if(p0[i] < min[i]) 
			min[i] = p0[i];
		if(p1[i] < min[i]) 
			min[i] = p1[i];
		if(p2[i] < min[i]) 
			min[i] = p2[i];
		if(p0[i] > max[i]) 
			max[i] = p0[i];
		if(p1[i] > max[i]) 
			max[i] = p1[i];
		if(p2[i] > max[i]) 
			max[i] = p2[i];
	}
	printf("MINIMUM: ");
	printf("\t(%4.2f %4.2f %4.2f)\n",min[0],min[1],min[2]);
	printf("MAXIMUM: ");
	printf("\t(%4.2f %4.2f %4.2f)\n",max[0],max[1],max[2]);
	*/
}

//int Triangle::trace(Ray &r) {
//    // r.P = origin point
//    // r.D = direction of ray
//    
//    pt3 edge1;
//    pt3 edge2;
//    pt3 edge3;
//    
//    pt3 tvec;
//    pt3 pvec;
//    pt3 qvec;
//    
//    double det,inv_det;
//    
//    double t;
//    double u;
//    double v;
//    
//    /* find vectors for two edges sharing vert0 (p0) */
//    pt3_vector_op(edge1,p1,-,p0);
//    pt3_vector_op(edge2,p2,-,p0);
//    
//    /* begin calculating determinant - also used to calculate U parameter */
//    pt3_cross(pvec,r.D,edge2);
//    
//    /*if the determinant is near zero, ray lies in plane of triangle */
//    det = pt3_dot(edge1,pvec);
//    
//    /* skipping culling! */
//    if(det > -EPSILON && det < EPSILON) {
//        return 0;
//    }
//    
//    inv_det = 1.0 / det;
//    
//    /* calculate distance from vert0 to ray origin */
//    pt3_vector_op(tvec,r.P,-,p0);
//    
//    /* calcuate U paramter and test bounds */
//    u = pt3_dot(tvec,pvec) * inv_det;
//    if(u < 0.0 || u > 1.0) {
//        return 0;
//    }
//    
//    /* prepare to test V paramter */
//    pt3_cross(qvec,tvec,edge1);
//    
//    /* calculate V paramter and test bounds */
//    v = pt3_dot(r.D,qvec) * inv_det;
//    if(v < 0.0 || u + v > 1.0) {
//        return 0;
//    }
//    
//    /* calcuate T, ray intersects triangle */
//    t = pt3_dot(edge2,qvec) * inv_det;
//    
//    // Calculate intersection point:
//    pt3 a,b,c;
//    
//    pt3_scalar_op(a,p0,*,1-u-v);
//    pt3_scalar_op(b,p1,*,u);
//    pt3_scalar_op(c,p2,*,v);
//    
//    pt3_vector_op(a,a,+,b);
//    pt3_vector_op(a,a,+,c);
//    
//    printf("Hit! %4.2f \n",pt3_distance(r.P,a));
//
//    
//    pt3_copy(r.intersection,a);
//    
//    return 1;
//    
//    
//}

int Triangle::trace(Ray &r) {
	pt3  e1,e2,h,s,q;
	float a,f,u,v;
	pt3_vector_op(e1,p1,-,p0);
	pt3_vector_op(e2,p2,-,p0);
    
	pt3_cross(h,r.D,e2);
	a = pt3_dot(e1,h);
    
	if (a > -0.00001 && a < 0.00001)
		return(0);
    
	f = 1/a;
	pt3_vector_op(s,r.P,-,p0);
	u = f * (pt3_dot(s,h));
    
	if (u < 0.0 || u > 1.0)
		return(0);
    
	pt3_cross(q,s,e1);
	v = f * pt3_dot(r.D,q);
    
	if (v < 0.0 || u + v > 1.0)
		return(0);
    
	// at this stage we can compute t to find out where
	// the intersection point is on the line
	float t = f * pt3_dot(e2,q);
    
	if (t > 0.00001) { // ray intersection
        // Calculate intersection point:
        pt3 a,b,c;
        
        pt3_scalar_op(a,p0,*,1-u-v);
        pt3_scalar_op(b,p1,*,u);
        pt3_scalar_op(c,p2,*,v);
        
        pt3_vector_op(a,a,+,b);
        pt3_vector_op(a,a,+,c);
        
        //printf("Hit! %4.2f \n",pt3_distance(r.P,a));
        
        pt3_copy(r.intersection,a);
		return(1);

    }
    
	else // this means that there is a line intersection
        // but not a ray intersection
        return (0);
    

}

color Triangle::getColor(Ray &r , double * camera, vector<Shape *> objects) {
	
	color aquamarine(127.0/255.0,255.0/255.0,212.0/255.0);
    
	return aquamarine;
    
}


AABB Triangle::getAABB() {
    AABB bb(p0);
    bb += p1;
    bb += p2;
    return bb;
}
