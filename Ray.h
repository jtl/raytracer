#ifndef _RAY_H_
#define _RAY_H_

#include "pt3.h"

/*
 * Ray class, for use with the optimized ray-box intersection test
 * described in:
 *
 *      Amy Williams, Steve Barrus, R. Keith Morley, and Peter Shirley
 *      "An Efficient and Robust Ray-Box Intersection Algorithm"
 *      Journal of graphics tools, 10(1):49-54, 2005
 * 
 */

class Ray {
  public:
    Ray() { }
    
    Ray(const Ray &other) {
        pt3_copy(P,other.P);
        pt3_copy(D,other.D);
    }

    // FOR RAY-BOX INTERSECTION
    
    pt3 inv_direction;
    int sign[3];
    
    void init_ray() {
        pt3_set(inv_direction,1/D[0],1/D[1],1/D[2]);
        sign[0] = (inv_direction[0] < 0);
        sign[1] = (inv_direction[1] < 0);
        sign[2] = (inv_direction[2] < 0);
    }
    
    // FROM ORIGINAL RAY

    pt3 P;
    pt3 D;

    pt3 intersection;
    pt3 normal;
};

#endif // _RAY_H_
