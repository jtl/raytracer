//
//  plyreader.h
//  Raytracer
//
//  Created by James Loomis on 9/28/12.
//  Copyright (c) 2012 James Loomis. All rights reserved.
//

#ifndef Raytracer_plyreader_h
#define Raytracer_plyreader_h

#include <stdio.h>
#include <math.h>
#include <strings.h>
#include <assert.h>
#include "ply.h"
#include "pt3.h"
#include "Triangle.h"
#include <vector>

typedef struct Vertex {
    float x,y,z;
    float r,g,b;
    float nx,ny,nz;
    void *other_props;       /* other properties */
} Vertex;

typedef struct Face {
    unsigned char nverts;    /* number of vertex indices in list */
    int *verts;              /* vertex index list */
    void *other_props;       /* other properties */
} Face;

Triangle *face2triangle(Face f);
void addToWorld(vector<Shape *> &world);
void read_ply_file(char *filename);
void ply_translate(float x,float y,float z);
void ply_scale(float s);


#endif
