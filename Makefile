# pipeline makefile

CC = g++

CFLAGS=-ggdb -I. 

# For Linux/Solaris:
#LDFLAGS=-lglut -lGL -lm
# For OS X:
LDFLAGS=-framework GLUT -framework OpenGL

all: clean main 

main: setPixel.o Sphere.o pt3.o quadratic.o Polygon.o Plane.o Shape.o color.o Triangle.o KDTree.o AABB.o ply.o plyreader.o CornellBox.o

color.o: setPixel.o

clean:
	rm -f main *.o

