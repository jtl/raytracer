//
//  AABB.cpp
//  Raytracer
//
//  Created by James Loomis on 9/25/12.
//  Copyright (c) 2012 James Loomis. All rights reserved.
//

#include "AABB.h"
#include <cassert>

#define MAX(a,b) (((a) > (b)) ? (a) : (b))
#define MIN(a,b) (((a) < (b)) ? (a) : (b))


void AABB::zero() {
    pt3_zero(min);
    pt3_zero(max);
}

AABB::AABB() {
    zero();
    assert("Default constructor on AABB");
}

AABB::AABB(const AABB &other) {
    pt3_copy(min,other.min);
    pt3_copy(max,other.max);
    assert(valid());
}

AABB::AABB(pt3 other_min,pt3 other_max) {
    pt3_copy(min,other_min);
    pt3_copy(max,other_min);
    this->operator+=(other_max);
    assert(valid());
}

AABB::AABB(pt3 point) {
    pt3_copy(min,point);
    pt3_copy(max,point);
}

double AABB::split(int axis,AABB &left,AABB &right) {
    double axismidpt = min[axis] + (max[axis]-min[axis])/2.0;
    pt3 leftmax;
    pt3 rightmin;
    pt3_copy(leftmax,max);
    pt3_copy(rightmin,min);
    leftmax[axis] = axismidpt;
    rightmin[axis] = axismidpt;
    
    left = min;
    left += leftmax;
    right = rightmin;
    right += max;
    
    return axismidpt;
}

bool AABB::intersects(AABB other) {
    /*
     FROM LECTURE NOTES:
     
     bool AABB_intersect (A, B) 
        for each axis x, y, z
            if (Aaxis.min > Baxis.max) or
               (Baxis.min > Aaxis.max) 
                return FALSE
        return TRUE;
     
     */
    for(int i=0;i<2;i++) {
        if(min[i] > other.max[i] ||
           other.min[i] > max[i]) {
            return false;
        }
    }
    return true;
}

/*
 * Ray-box intersection using IEEE numerical properties to ensure that the
 * test is both robust and efficient, as described in:
 *
 *      Amy Williams, Steve Barrus, R. Keith Morley, and Peter Shirley
 *      "An Efficient and Robust Ray-Box Intersection Algorithm"
 *      Journal of graphics tools, 10(1):49-54, 2005
 *
 */

bool AABB::intersects(Ray &r, float t0, float t1) {
    
    r.init_ray();
    pt3 *parameters[2];
    parameters[0] = &min;
    parameters[1] = &max;
    
    tmin = tminv[0] = ((*(parameters[r.sign[0]]))[0] - r.P[0]) * r.inv_direction[0];
    tmax = tmaxv[0] = ((*(parameters[1-r.sign[0]]))[0] - r.P[0]) * r.inv_direction[0];
    tminv[1] = ((*(parameters[r.sign[1]]))[1] - r.P[1]) * r.inv_direction[1];
    tmaxv[1] = ((*(parameters[1-r.sign[1]]))[1] - r.P[1]) * r.inv_direction[1];
    if ( (tmin > tmaxv[1]) || (tminv[1] > tmax) )
        return false;
    if (tminv[1] > tmin)
        tmin = tminv[1];
    if (tmaxv[1] < tmax)
        tmax = tmaxv[1];
    tminv[2] = ((*(parameters[r.sign[2]]))[2] - r.P[2]) * r.inv_direction[2];
    tmaxv[2] = ((*(parameters[1-r.sign[2]]))[2] - r.P[2]) * r.inv_direction[2];
    if ( (tmin > tmaxv[2]) || (tminv[2] > tmax) )
        return false;
    if (tminv[2] > tmin)
        tmin = tminv[2];
    if (tmaxv[2] < tmax)
        tmax = tmaxv[2];
    
    return ( (tmin < t1) && (tmax > t0) );
}


AABB & AABB::operator+=(const AABB &other) {
    /*
    for(int i=0;i<3;i++) {
        if(other.min[i] < min[i])
            min[i] = other.min[i];
        if(other.max[i] > max[i])
            max[i] = other.max[i];
    }
    */
    (*this) += other.max;
    (*this) += other.min;
    assert(valid());
    return *this;
}

AABB & AABB::operator=(const pt3 point) {
    pt3_copy(min,point);
    pt3_copy(max,point);
    return *this;
}

AABB & AABB::operator+=(const pt3 point) {
    for(int i=0;i<3;i++) {
        if(point[i] < min[i])
            min[i] = point[i];
        if(point[i] > max[i])
            max[i] = point[i];
    }
    assert(valid());
    return *this;
}

const AABB AABB::operator+(const AABB &other) const {
    AABB result(*this);
    result += other;
    return result;
}

bool AABB::valid() {
    for(int i=0;i<3;i++) {
        if(min[i] > max[i]) {
            printf("MIN: (%2.2f , %2.2f, %2.2f) MAX: (%2.2f  , %2.2f , %2.2f)\n",min[0],min[1],min[2],max[0],max[1],max[2]);
            return false;
        }
    }
    return true;
}

bool AABB::intersect2(const Ray& inRay, float& outDst) {
    static const float EPSILON = 0.00001;
    
	const float ox=inRay.P[0], oy=inRay.P[1], oz=inRay.P[2];
	const float dx=inRay.D[0], dy=inRay.D[1], dz=inRay.D[2];
    
	float tx_min, ty_min, tz_min;
	float tx_max, ty_max, tz_max;
    
	// x
	float a = 1.f/dx;
	if(a >= 0)
	{
		tx_min = (min[0]-ox)*a;
		tx_max = (max[0]-ox)*a;
	}
	else
	{
		tx_min = (max[0]-ox)*a;
		tx_max = (min[0]-ox)*a;
	}
    
	// y
	float b = 1.f/dy;
	if(b >= 0)
	{
		ty_min = (min[1]-oy)*b;
		ty_max = (max[1]-oy)*b;
	}
	else
	{
		ty_min = (max[1]-oy)*b;
		ty_max = (min[1]-oy)*b;
	}
    
	// z
	float c = 1.f/dz;
	if(c >= 0)
	{
		tz_min = (min[2]-oz)*c;
		tz_max = (max[2]-oz)*c;
	}
	else
	{
		tz_min = (max[2]-oz)*c;
		tz_max = (min[2]-oz)*c;
	}
    
	float t0, t1;
    
	// find largest entering t-value
	t0 = MAX(tx_min, ty_min);
	t0 = MAX(tz_min, t0);
    
	// find smallest exiting t-value
	t1 = MIN(tx_max, ty_max);
	t1 = MIN(tz_max, t1);
    
	//if(t0 < t1 && t1 > EPSILON)
    if(t0 < t1)
	{
		outDst = t0;
		return true;
	}
	return false;
}
