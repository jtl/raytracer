
#include "Ray.h"
#include "color.h"
#include <vector>
#include "AABB.h"

#ifndef __SHAPE_H

#define __SHAPE_H

class Shape {

public:
    Shape();
    virtual int trace(Ray &ray);
    void setColor(double r,double g,double b);
    void setAmbient(double r,double g,double b);
    void setDiffuse(double r,double g,double b);
    void setSpecular(double r,double g,double b);
    virtual color getColor(Ray &r, pt3 camera, vector<Shape *> objects);
    color getColor(Ray &r , int depth, pt3 camera, vector<Shape *> objects);
    bool inShadow(Ray r ,pt3 lightposition, vector<Shape *> objects);
    void setMaterialConstants(double _k_a,double _k_d,double _k_s,double _k_e,double _k_r,double _k_t, double _n);

    float shininess;

    virtual AABB getAABB();

//private:
    //pt3 color;
    color ambient;
    color diffuse;
    color specular;

public:
    double k_a,k_d,k_s,k_e,k_r,k_t,n;

};

#endif
