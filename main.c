/*
 *  2DPipeline
 *
 *  This program tests the 2D rendering pipeline.  
 *  
 *
 */

#include <stdlib.h>

#ifdef __APPLE__
#include <GLUT/glut.h>         /* glut.h includes gl.h and glu.h*/
#else
#include <GL/glut.h>
#endif

#include "setPixel.h"
#include "raytrace.cpp"

int screenHeight = 500;
int screenWidth = 500;

/*
 * Callback for keystrokes
 */
void key (unsigned char key, int x, int y)
{
	
	if ((key == 'Q') || (key == 'q')) exit(0); // quit

	
	glutPostRedisplay();
}


/* 
* Callback function for mouse interaction 
 */

void mouse( int button, int state, int x, int y ) {
	
	/* 
    * If state is not checked, we get two events for each mouse button
	 * usage, one for pressing it, one for releasing it 
	 */
	if ( state==GLUT_UP ) {
		//displayNumber++;
		glutPostRedisplay();
	}
	
}


/* This will display the original world when the displaynumber is
* odd and an altered world when it is even.  "gluOrtho2D" is used
* to determine which portion of the world is displayed. "glViewport"
* is used to determine WHERE the selected portion will be displayed.
*/

void display( void ) {
	
	/* 
	* Set clear color to gray 
     */
    glClearColor ( 0.0, 0.0, 1.0, 0.0 );
	
    /* 
	* Clear screen window 
     */
    glClear(GL_COLOR_BUFFER_BIT);

	/*
	for(int i=0;i<500;i++) {
		double j = (double)i / 500.0;
		setPixel(i,i,j,0.0,1.0-j);
	}
	*/
	raytrace();

	
	/* 
     * Flush GL buffers to the display   
     */
    glFlush( );
	
}


/* 
 * Main routine - GLUT setup and initialization 
 */

int main( int argc, char** argv ) {
	
	/* 
     * Initialize mode and open a window in upper 
	 * left corner of screen 
	 */
	
	glutInit( &argc, argv ); 
	glutInitDisplayMode( GLUT_SINGLE | GLUT_RGB );
	glutInitWindowSize( screenWidth, screenHeight );
	glutInitWindowPosition( 0, 0 );
	glutCreateWindow( "Draw World" ); 
	
	glutMouseFunc( mouse );
	glutKeyboardFunc ( key );
	glutDisplayFunc( display );
	
	glutMainLoop();
	
	return 0;
	
}
