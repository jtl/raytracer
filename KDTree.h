//
//  KDTree.h
//  Raytracer
//
//  Created by James Loomis on 9/25/12.
//  Copyright (c) 2012 James Loomis. All rights reserved.
//

#ifndef __Raytracer__KDTree__
#define __Raytracer__KDTree__

#include <iostream>

#define MIN_SIZE 3

#include "Shape.h"
#include <vector>

class KDTree {
public:
    KDTree(vector<Shape *> objects);
    KDTree(vector<Shape *> objects,AABB bb,int depth);
    ~KDTree();
    Shape *firstIntersection(Ray &r);
    int size();
    
private:
    void getNode(int depth);
private:
    vector<Shape *> world;
    bool isleaf;
    int sepaxis;
    double sepplane;
    KDTree *left;
    KDTree *right;
public:
    AABB voxel;
};

#endif /* defined(__Raytracer__KDTree__) */
