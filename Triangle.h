//
//  Triangle.h
//  Raytracer
//

#ifndef Raytracer_Triangle_h
#define Raytracer_Triangle_h

#include "Shape.h"

#define EPSILON 0.000001

class Triangle : public Shape {
public:
    Triangle(pt3 _p0,pt3 _p1,pt3 _p2);
    Triangle(float,float,float,float,float,float,float,float,float);
    void print();
    int trace(Ray &r);
    color getColor(Ray &r , double * camera, vector<Shape *> objects);
    AABB getAABB();
private:
    pt3 p0;
    pt3 p1;
    pt3 p2;

};

#endif
