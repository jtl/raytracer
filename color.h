////
//// FILE: color.h
////
//// class for holding a color value.
//// contains various functions / overloaded operators for
//// performing operations on colors.
////
////

#include <iostream>
using namespace std;

#ifndef _COLOR_H
#define _COLOR_H

class color {
  public:
	color(float r,float g,float b);
	color(double *a);
	color();
	float r;
	float g;
	float b;
	color  operator- (const color& c);
	color  operator+ (const float& c);
	color  operator* (const color& c);
	color  operator/ (const float& f);
	color  operator/ (const color& f);
	color  operator* (const float& f);
	color& operator=(const color& c);
	color& operator*= (const float& f);
	color& operator+=(const color& c);
	color& operator/=(const float& f);

	void max();
	double luminance();
	
	void set(float r,float g,float b);
};

color getcolor();
color getclearcolor();
void setcolor(float,float,float);
void setclearcolor(float,float,float);

void setPixel(int,int,color c);

ostream& operator<<(ostream& os, const color& c);

#endif
