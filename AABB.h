//
//  AABB.h
//  Raytracer
//
//  Created by James Loomis on 9/25/12.
//  Copyright (c) 2012 James Loomis. All rights reserved.
//

#ifndef __Raytracer__AABB__
#define __Raytracer__AABB__

#include <iostream>

#include "Ray.h"
#include "pt3.h"

class AABB {
public:
    AABB(const AABB &other);
    AABB(pt3 other_min,pt3 other_max);
    AABB(pt3 point);
    AABB();
    bool intersects(AABB other);
    bool intersects(Ray &r, float t0, float t1);
    bool intersect2(const Ray& inRay, float& outDst);
    const AABB operator+(const AABB &other) const;
    AABB & operator+=(const AABB &other);
    AABB & operator+=(const pt3 point);
    AABB & operator=(const pt3 point);
    double split(int axis,AABB &one,AABB &two);
    bool valid();
    
private:
    pt3 min;
    pt3 max;
    void zero();
   
public: // For intersection with Ray calculation:
    float tmin, tmax;
    pt3 tminv, tmaxv;
};

#endif /* defined(__Raytracer__AABB__) */
