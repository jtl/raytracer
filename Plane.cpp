//// 
//// FILE: Plane.cpp
////
//// For intersection calculations with an infinite plane.
////
////
////
////
////
////
////

#include "Plane.h"
#include <math.h>
#include "quadratic.h"

// From: 
// http://www.cs.rit.edu/~wrc/courses/common/notes/cg2/2-2b-rt-basics-2.pdf
// Starting on page 25
int Plane::trace(Ray &r) {

	double omega = -( pt3_dot(this->normal,r.P) + this->F ) / pt3_dot(this->normal,r.D);
	if(omega <= 0) {
		// No intersection
		return 0;
	} else {
		// Compute intersection point
		// Page 28 in slides
		pt3_scalar_op(r.intersection,r.D,*,omega);
		pt3_vector_op(r.intersection,r.intersection,+,r.P);
		pt3_copy(r.normal,this->normal);
		return 1;
	}


}


color Plane::getColor(Ray &r , double * camera, vector<Shape *> objects) {
	
	color aquamarine(127.0/255.0,255.0/255.0,212.0/255.0);

	return aquamarine;

}
