
#include "Shape.h"

class Sphere : public Shape {
	public:
		Sphere(double x,double y,double z,double r);
		int trace(Ray &r);
        AABB getAABB();
	private:
		pt3 c; // center
		double r; // radius
};
