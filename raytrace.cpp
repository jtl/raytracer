//// 
//// FILE: raytrace.cpp
////
//// This includes the raytracing for each screen pixel, as well as 
//// tone reproduction operators near the end of the function.
////
////
////
////
////
////

#include <time.h>
#include <math.h>
#include "Sphere.h"
#include "Polygon.h"
#include "Plane.h"
#include "Triangle.h"
#include "setPixel.h"
#include "KDTree.h"
#include "plyreader.h"
#include <vector>
#include <cstdlib>
#include "Timer.h"
#include "CornellBox.h"

#include <iostream>

#define RAND_FLOAT ((float)rand() / (float)RAND_MAX)

#define PI 3.14159
#define SAMPLES 1
#define PHOTONS 100

using namespace std;

#define WARD true

/*
double Lmax = 1000.0; 
double Ldmax = 100.0; // 100 nits

double L[500][500];
color RGB[500][500];
*/

color backgroundcolor(0.0,.2,.6);
color foregroundcolor(1.0,0.0,0.0);

void raytrace() {

    srand((unsigned)time(0));
    
    vector<Shape *> objects;
    vector<Shape *> quads;
    add_cornell_shortbox(quads);
    add_cornell_tallbox(quads);
    add_cornell_box_walls(quads);
    for(int i=0;i<quads.size();i++) {
        Polygon *p = (Polygon *)quads.at(i);
        p->split(objects);
    }
    printf("size: %d\n",objects.size());

	Timer timer;
	//timer.start("KDTree Construction");
    //KDTree kd_orig(objects);
	//timer.done();
    
    timer.start("Creating Photon Map...");
    //PhotonMap pm(objects);
    
    //// PhotonMapping /////
    /* LIGHT:
     -0.24,1.98,0.16,
     -0.24,1.98,-0.22,
     0.23,1.98,-0.22,
     0.23,1.98,0.16);
     */
    vector<Shape *> photonlist;
    for(int i=0;i<PHOTONS;i++) {
        //timer.completed(i);
        
        pt3 light;
        
        light[0] = (.47 ) + -0.24;
        light[1] = 1.97;
        light[2] = (.38 ) + -0.22;
        
        //light[0] = 0.0;
        //light[1] = 1.0;
        //light[2] = 2.0;
        
        // Direction
        // FROM:
        // http://mathworld.wolfram.com/SpherePointPicking.html
        
        float u = RAND_FLOAT;
        float v = RAND_FLOAT;
        
        float theta = 2 * PI * u;
        float phi = acos( 2 * v - 1 );
        
        pt3 lightdir;
        //pt3_copy(light,lightdir);
        lightdir[0] = cos(theta) * sin(phi);
        lightdir[1] = sin(theta) * sin(phi);
        lightdir[2] = sin(phi);
        // NOT correctly distributed, but should at least work..
        
        //double o = 10.0 * tan( 22.5 * PI / 180.0 );
        
        normalize(lightdir);    
        
        float dist = 10000.0;
        pt3 intersection;
        Shape *hit_shape = 0;
        
        for(int shape=0;shape<objects.size();shape++) {
            Ray r;
            pt3_copy(r.P,light);
            pt3_copy(r.D,lightdir);
            normalize(r.D);
            
            Shape *s = objects.at(shape);
            int hit = s->trace(r);
            
            if(hit > 0) {
                double d = pt3_distance(light,r.intersection);
                if(d < dist) {
                    pt3_copy(intersection,r.intersection);
                    dist = d;
                    hit_shape = s;
                }
            }
        }
        
        if(dist > 9999.0) { // No intersection - let's not count this photon...
            i--;
            printf(" (%d) No intersection: %4.2f %4.2f %4.2f\n",i,lightdir[0],lightdir[1],lightdir[2]);
        } else {
            
            Sphere *photon_sphere = new Sphere(
                intersection[0],
                intersection[1],
                intersection[2],
                0.1);
            photon_sphere->ambient = hit_shape->ambient;
            photonlist.push_back(photon_sphere);
            
        }        
    }
    
    timer.done();
    
    //exit(0);
    timer.start("creating photon kd-tree");
    
    /*
    for(int i=0;i<objects.size();i++) {
        Shape *s = objects.at(i);
        s->ambient *= .5;
        
        photonlist.push_back(s);
    }
     */
    
    KDTree kd(objects);
    timer.done();
    
    printf("Rendering %d objects...\n",(int)kd.size());


    // distance to screen edge from center
		double o = 10.0 * tan( 22.5 * PI / 180.0 );
		
        pt3 eye;
		eye[0] = 0.0;
		eye[1] = 1.0;
		eye[2] = 2.0;

        pt3 topleft;
		topleft[0] = -o;
		topleft[1] = o;
		topleft[2] = 0.0;

		pt3 bottomright;
		bottomright[0] = o;
		bottomright[1] = -o;
		bottomright[2] = 0.0;

		int width = 500;
		int height = 500;

		pt3 lookat;
		lookat[0] = 0.0;
		lookat[1] = -1.0;
		lookat[2] = -2.0;

		double sampledist = o / (double)width / 2;

		timer.start("Raytracing!");
    
		for(int i=width;i>0;i--) {


			for(int j=0;j<height;j++) {

				color *colors = new color[SAMPLES];
                                
				for(int k=0;k<SAMPLES;k++) {
					double dist = 9999.0;
                    
                    if(SAMPLES > 1) {
                        // ANTI ALIASING USING RANDOM SAMPLES:
                        
                        double randX = ((double)rand() / (double)RAND_MAX) + (double)i;
                        double randY = ((double)rand() / (double)RAND_MAX) + (double)j;
                        
                        lookat[0] = ((randX - width/2.0)/(width/2.0)) * o;
                        lookat[1] = ((randY - height/2.0)/(height/2.0)) * o;
                    } else {
                        lookat[0] = (((double)i - width/2.0)/(width/2.0)) * o;
                        lookat[1] = (((double)j - height/2.0)/(height/2.0)) * o;
                    }

                    colors[k] = backgroundcolor;
                    
                    /*
                    for(int shape=0;shape<objects.size();shape++) {
						Ray r;
						pt3_copy(r.P,eye);
						pt3_vector_op(r.D,lookat,-,eye);
						normalize(r.D);
                        
						Shape *s = objects.at(shape);
						int hit = s->trace(r);
                        
						if(hit > 0) {
							double d = pt3_distance(eye,r.intersection);
							if(d < dist) {
								dist = d;
								colors[k] = s->ambient;
							}
						}
					}
                     */
                    
                    
                    Ray r;
                    pt3_copy(r.P,eye);
                    pt3_vector_op(r.D,lookat,-,eye);
                    normalize(r.D);
                    
                    Shape *s = kd.firstIntersection(r);
                    if(s) s->trace(r);
                    
					if(s) { colors[k] = s->ambient / pt3_distance(eye,r.intersection);
                    } else    colors[k] = backgroundcolor;
				}

				color c(0.0,0.0,0.0);

				for(int k=0;k<SAMPLES;k++) {
					c += colors[k];
				}
				c /= (float)SAMPLES;

				delete [] colors;
				

				// Draw directly to screen:
				setPixel(i,j,c);

				// Save for later:
                
                /*
				RGB[i][j] = c;

				RGB[i][j] *= Lmax;
				L[i][j] = RGB[i][j].luminance();		
                */
			}

		}
		timer.done();
	
		/** START WARD **/
    
        /*
		double Lsum = 0;
		double small = 0.0001;
		for(int i=0;i<500;i++) {
			for(int j=0;j<500;j++) {
				Lsum += log(small + L[i][j]);
			}
		}
		double Lwa = exp( Lsum / (500 * 500) );
		
		if(WARD) {
			double sf = pow((1.219 + pow((Ldmax / 2),0.4)) / (1.219 + pow(Lwa,0.4)) , 2.5);

			for(int i=0;i<500;i++) {
				for(int j=0;j<500;j++) {
					RGB[i][j] *= sf;
					RGB[i][j] /= Ldmax;
					RGB[i][j].max();
				}
			}
		} else {
			double a = 0.18;
			for(int i=0;i<500;i++) {
				for(int j=0;j<500;j++) {
					RGB[i][j] *= a;
					RGB[i][j] /= Lwa;
					RGB[i][j].max();
					RGB[i][j] = (RGB[i][j] / (RGB[i][j] + 1.0));
				}
			}
		}

		for(int i=0;i<500;i++) {
			for(int j=0;j<500;j++) {
				setPixel(i,j,RGB[i][j]);
			}
		}
    
        */
	


}
		
		
		
		
