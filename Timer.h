
#ifndef __TIMER_H
#define __TIMER_H

#include <time.h>

class Timer {

public:
	Timer() {} 
	void start(char* message) { 
		msg = message;
		printf("%s started.\n",msg); 
		initial = time(NULL); 
	}
	void progress(double percent) {
		time_t elapsed = time(NULL) - initial;
		time_t total = elapsed / percent;
		time_t remaining = total - elapsed;
		printf("\r remaining: ");
		print(remaining);
	}
    void completed(int c) {
        printf("\r completed: %d",c);
    }
	void print(time_t t) {
		int seconds = t % 60;
		int minutes = ((t-seconds) / 60) % 60;
		int hours = (t-(minutes*60)) / 3600;
		int hrs = (int)(t / 3600);

		printf("%3d hrs %3d min %3d sec",
			hours,minutes,seconds);
	}
	void done() {
		printf("\n\n%s completed.\n total time: ",msg);
		print(time(NULL) - initial);
		printf("\n");
	}

private:
	time_t initial;
	char * msg;
};

#endif // __TIMER_H
