////
//// FILE: pt3.h
////
//// Contains a typedef of pt3 with a double array.
//// Various preprocessor definitions are here for 
//// calculations on vectors of size 3. Used for
//// vector calculations in the raytracer.
////
////

#include <math.h>

typedef float pt3[3];

#define pt3_vector_op(c,a,op,b) for(int i=0;i<3;i++) c[i] = a[i] op b[i]
#define pt3_scalar_op(c,a,op,b) for(int i=0;i<3;i++) c[i] = a[i] op b
#define pt3_zero(c) for(int i=0;i<3;i++) c[i] = 0
#define pt3_copy(c,a) for(int i=0;i<3;i++) c[i] = a[i]
#define pt3_cross(c,a,b) c[0] = a[1] * b[2] - a[2] * b[1]; c[1] = a[2] * b[0] - a[0] * b[2]; c[2] = a[0] * b[1] - a[1] * b[0]
#define pt3_dot(a,b) (a[0]*b[0] + a[1]*b[1] + a[2]*b[2])
#define pt3_square_sum(a) ( pow(a[0],2.0) + pow(a[1],2.0) + pow(a[2],2.0) )
#define pt3_distance(a,b) ( sqrt( pow(a[0]-b[0],2.0) + pow(a[1]-b[1],2.0) + pow(a[2]-b[2],2.0) ) )
#define pt3_set(a,x,y,z) a[0] = x; a[1] = y; a[2] = z

void normalize(pt3& v);
