
#include "Shape.h"

class Plane : public Shape {
	public:
		//Plane(double x,double y,double z,double F);
		int trace(Ray &r);
		color getColor(Ray &r , double * camera, vector<Shape *> objects);

		pt3 normal; // normal vector of plane
		double F; // shortest distance to plane
};
