////
//// FILE: Polygon.cpp
////
//// For intersection calculations with a polygon (with 4 vertices)
////
//// Procedural shading from checkpoint 4 is included here.
////
////
////
////

#include "Plane.h"
#include "Polygon.h"
#include <math.h>
#include "quadratic.h"

#include <iostream>

#define PI 3.14159265


Polygon::Polygon(float x1,float y1,float z1,
                 float x2,float y2,float z2,
                 float x3,float y3,float z3,
                 float x4,float y4,float z4) {
    
    t1 = new Triangle(x1,y1,z1,x2,y2,z2,x3,y3,z3);
    t2 = new Triangle(x1,y1,z1,x3,y3,z3,x4,y4,z4);
    
    setColor(127.0/255.0,255.0/255.0,212.0/255.0);

}

Polygon::Polygon(pt3 _a,pt3 _b,pt3 _c,pt3 _d) {
    cout << "Broken" <<endl;
    assert(false);
}

double anglebetween(double a[],double b[]) {
	//return acos( pt3_dot(a,b) / (sqrt( pt3_dot(a,a) ) * sqrt( pt3_dot(b,b) )) );
    cout << "Broken" <<endl;
    assert(false);
    return 0.0;
    
}

int Polygon::trace(Ray &r) {
    return t1->trace(r) || t2->trace(r);
}

void Polygon::split(vector<Shape *> & objects) {

    t1->ambient = ambient;
    t1->diffuse = diffuse;
    t1->specular = specular;
    
    t2->ambient = ambient;
    t2->diffuse = diffuse;
    t2->specular = specular;
    
    t1->setMaterialConstants( k_a,k_d,k_s,k_e,k_r,k_t,n );
    t2->setMaterialConstants( k_a,k_d,k_s,k_e,k_r,k_t,n );

    objects.push_back(t1);
    objects.push_back(t2);
}

AABB Polygon::getAABB() {
    AABB bb = t1->getAABB() + t2->getAABB();
    return bb;
}
