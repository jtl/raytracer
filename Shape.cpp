////
//// FILE: Shape.cpp
////
//// For intersection calculations with any shape. 
//// All other objects in the scene are derived classes
//// of this shape class.
////
//// Transmission and reflection rays are spawned from the getColor() function
//// for checkpoints 5 and 6.
//// 
//// Phong shading is also included here.
////
////

#include "Shape.h"

#define MAX(a,b) (((a) > (b)) ? (a) : (b))

#define MAX_DEPTH 4 
#include <math.h>
#include <cassert>

extern color backgroundcolor;

Shape::Shape() {
    k_r = 0.0;
    k_a = .3;
    k_d = .4;
    k_s = .3;
}

int Shape::trace(Ray &ray) {
	return 0;
}

void Shape::setMaterialConstants(double _k_a,double _k_d,double _k_s,double _k_e,double _k_r,double _k_t, double _n) {
		k_a = _k_a; 
		k_d = _k_d; 
		k_s = _k_s; 
		k_e = _k_e; 
		k_r = _k_r; 
		k_t = _k_t;
		n = _n;
}

void Shape::setColor(double r,double g,double b) {
	//pt3_set(this->color,r,g,b);
	ambient.set(r,g,b);	
	diffuse.set(r,g,b);
	specular.set(r,g,b);
}

void Shape::setAmbient(double r,double g,double b) {
	ambient.set(r,g,b);	
}

void Shape::setDiffuse(double r,double g,double b) {
	diffuse.set(r,g,b);
}

void Shape::setSpecular(double r,double g,double b) {
	specular.set(r,g,b);
}

void reflect(pt3 R,pt3 S,pt3 N) {
		float x = 2.0 * (pt3_dot(S,N) / pt3_dot(N,N));
		pt3 rightTerm;
		pt3_scalar_op(rightTerm,N,*,x);
		pt3_vector_op(R,S,-,rightTerm);
}
void reflect_old(pt3 Rr,pt3 Ri,pt3 N) {
		// Rr = Ri - 2 N (Ri . N)
		float RidotN = 2 * pt3_dot(Ri,N);
		pt3 rightTerm;
		pt3_scalar_op(rightTerm,N,*,RidotN);
		pt3_vector_op(Rr,Ri,-,rightTerm);
}

bool transmit( pt3 Rr,pt3 D,pt3 _N, double k_t, double n ){

        double Nit = 1.0/n;
        pt3 leftterm;
        pt3_scalar_op(leftterm,D,*,Nit);

		pt3 minusD;
		pt3_scalar_op(minusD,D,*,-1.0);

        double DdotN = pt3_dot(minusD,_N);
		pt3 N;

		if(DdotN < 0) { // inside?
			Nit = k_t;
			pt3_scalar_op(N,_N,*,-1.0);
		} else {
			pt3_copy(N,_N);
		}
		DdotN = pt3_dot(minusD,N);

        double middleterm = Nit * DdotN;
        double disc = 1.0+((Nit*Nit)*(DdotN*DdotN-1.0));
		double rightterm = sqrt(disc);
        double right = middleterm -rightterm;
        pt3 rightdotN;
        pt3_scalar_op(rightdotN,N,*,right);
        
        pt3_vector_op(Rr,leftterm,+,rightdotN);

		bool TIR = disc < 0.0;
		return TIR;

}


bool Shape::inShadow (Ray r,pt3 lightposition,vector<Shape *> objects) {
	// check if in a shadow
	bool inshadow = false;
	for(int i=0;i<objects.size();i++) {
		// No need to check this object
		if(objects.at(i) == this) continue;

		// Create shadow ray from point to light source
		Ray shadowRay;
		pt3_copy(shadowRay.P,r.intersection);
		pt3_vector_op(shadowRay.D,lightposition,-,r.intersection);
		normalize(shadowRay.D);

		// Distance to light source:
		double lightdist = pt3_distance(r.intersection, lightposition);

		// Cast shadow ray
		Shape *s = objects.at(i);
		int hit = s->trace(shadowRay);
		double shapedist = pt3_distance(r.intersection,shadowRay.intersection);
		double shadowdist = pt3_distance(lightposition,shadowRay.intersection);

		if(hit && shapedist < lightdist && shadowdist < lightdist) {
			inshadow = true;
		}
	
	}
	return inshadow;
}


color Shape::getColor(Ray &r, int depth, pt3 camera, vector<Shape *>objects) {
	color retcolor = this->getColor(r,camera,objects);
	if(depth < MAX_DEPTH) {
			bool TIR = false;
			if(k_t > 0) {
		
					color transmittedcolor(0.0,0.0,0.0);        //trsansmitted ray

					pt3 T;
					
					//reflect_old(R,r.D,r.normal);
					TIR = transmit(T,r.D,r.normal,k_t,n);
					

					normalize(T);
					if( ! TIR ) {
					double dist = 9999.0;
					for(int shape=0;shape<objects.size();shape++) {
						Shape *s = objects.at(shape);

						// don't perform reflection calculations for
						// this object
						if(this == s) continue;

						Ray reflectRay;
						Ray transmitRay;
						pt3_copy(transmitRay.P,r.intersection);
						pt3_copy(transmitRay.D,T);

						int hit = s->trace(transmitRay);

						if(hit > 0) {
							double d = pt3_distance(transmitRay.intersection,r.intersection);
							if(d < dist) {
								dist = d;
								transmittedcolor = s->getColor(transmitRay,depth+1,transmitRay.P,objects) * k_t;
							}
						}
					}
					if(dist > 9990.0) transmittedcolor = backgroundcolor * k_t;

					retcolor += transmittedcolor;
					}

			}
			
			if(k_r > 0 || TIR) {
					
					color reflectedcolor(0.0,0.0,0.0);

					pt3 R;
					
					reflect(R,r.D,r.normal);
					

					normalize(R);

					double dist = 9999.0;
					for(int shape=0;shape<objects.size();shape++) {
						Shape *s = objects.at(shape);

						// don't perform reflection calculations for
						// this object
						if(this == s) continue;

						Ray reflectRay;
						pt3_copy(reflectRay.P,r.intersection);
						pt3_copy(reflectRay.D,R);

						int hit = s->trace(reflectRay);

						if(hit > 0) {
							double d = pt3_distance(reflectRay.intersection,r.intersection);
							if(d < dist) {
								dist = d;
								reflectedcolor = s->getColor(reflectRay,depth+1,reflectRay.P,objects);
							}
						}
					}
					if(dist > 9990.0) reflectedcolor = backgroundcolor;
					
					// check for total internal reflection
					retcolor += reflectedcolor * (TIR ? k_r + k_t : k_r);

			}
			
			
			
			
			
	}
	retcolor.max();
	return retcolor;
}


color Shape::getColor(Ray &r,pt3 camera,vector<Shape *> objects) {
    
	color c;
	color white(1.0,1.0,1.0);
    
	float NdotS;
    
	//normalize(r.normal);
    
	//color = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
	c = ambient * k_a;
    
	for(int light = 0;light<1;light++) {
        
        pt3 lightposition;
        if(light == 0) {
            pt3_set(lightposition,0.0,1.0,2.0);
        } else if(light == 1) {
            pt3_set(lightposition,-8.0,13.0,13.0);
        }
        color lightspecular(1.0,1.0,1.0);
        color lightdiffuse(1.0,1.0,1.0);
        
        pt3 S;
        //pt3_vector_op(S,r.intersection,-,lightposition);
        pt3_vector_op(S,lightposition,-,r.intersection);
        normalize(S);
        
        if(true) {
        //if(! inShadow(r,lightposition,objects)) {
            NdotS = MAX( pt3_dot(r.normal,S), 0.0 );
            
            c += (diffuse * lightdiffuse) * NdotS * k_d;
			// (diffuse * _LightSource[0].diffuse);
            
            pt3 V;
            pt3_vector_op(V,r.intersection,-,camera);
            normalize(V);
            
            pt3 R;
            reflect_old(R,S,r.normal);
            normalize(R);
            
            float RdotV = pow( MAX(0.0,pt3_dot(R,V)), k_e ) ;
            
            c += (lightspecular * specular) * RdotV * k_s;
        }
        /*
         if( NdotL > 0.0) {
         pt3 view;
         pt3 reflection;
         float RdotV;
         pt3_vector_op(view,camera,-,r.intersection);
         normalize(view);
         //pt3_scalar_op(lightdir,lightdir,*,-1);
         pt3_scalar_op(S,S,*,-1);
         reflect(reflection,S,r.normal);
         normalize(reflection);
         
         RdotV = MAX( pt3_dot(reflection,view) , 0.0 );
         
         c += white * pow(RdotV, (0.20 * 128.0) );
         }
         */
        
        /*
         if( NdotL > 0.0 )
         {
         vec3 view, reflection;
         float RdotV;
         view = vec3( -normalize(gl_ModelViewMatrix * gl_Vertex) );
         reflection = normalize( reflect(-lightdir, normal) );
         RdotV = max( dot( reflection, view ), 0.0 );
         color += gl_FrontMaterial.specular *
         gl_LightSource[0].specular * 
         pow( RdotV, gl_FrontMaterial.shininess );
         }
         
         gl_FrontColor = color;
         gl_Position = ftransform();
         */
	}
	c.max();
	return c;
	
}

AABB Shape::getAABB() {
    assert("Can't get AABB of Shape!");
    return AABB();
}


