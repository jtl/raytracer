
#include "quadratic.h"

/*
 * Better way to compute roots of quadratic equation
 * http://www.siggraph.org/education/materials/HyperGraph/math/math1.htm
 */

// From: 
// http://stackoverflow.com/questions/1903954/
// is-there-a-standard-sign-function-signum-sgn-in-c-c
#define sgn(x) ( (x > 0) - (x < 0) )

// Assumes there are 2 roots. Don't use this function 
// if there aren't.
void quadratic(double roots[],double a,double b,double c) {
	// q = -0.5 [b +sgn(b) (b2 - 4ac) .5]
	double q = -0.5 * ( b + sgn(b) * (pow(b,2.0) - 4.0 * a * c) * .5 );

	// x1 = q/a;  x2 = c/q 	
	roots[0] = q / a;
	roots[1] = c / q;
	
}
