////
//// FILE: Sphere.cpp
////
//// For intersection calculations with a sphere
////
////
////
////
////
////

#include "Sphere.h"
#include <math.h>
#include "quadratic.h"


Sphere::Sphere(double x,double y,double z,double _r) : r(_r) {
	c[0] = x;
	c[1] = y;
	c[2] = z;
}

// From: 
// http://www.cs.rit.edu/~wrc/courses/common/notes/cg2/2-2b-rt-basics-2.pdf
// Starting on page 19
int Sphere::trace(Ray &r) {

	pt3 diff;
	pt3_vector_op(diff,r.P,-,this->c);

	double A = pt3_square_sum(r.D);
	double B = 2 * pt3_dot(r.D,diff);
	double C = pt3_square_sum(diff) - pow(this->r,2.0);

	double disc = pow(B,2.0) - 4.0 * C;

	int solutions = 0;
	if(disc == 0) {
		// ray intersects tangent to surface
		solutions = 1;
	} else if (disc > 0) {
		solutions = 2;
		// two roots - 
		// determine roots + use "nearest."
		double roots[2];
		double root;
		quadratic(roots,A,B,C);
		//if(roots[0] < 0.0 && roots[1] < 0.0) {
		//	root = roots[0] > roots[1] ? roots[0] : roots[1];
		//} else {
		//	root = roots[0] < roots[1] ? roots[0] : roots[1];	
		//}
		root = roots[0] < roots[1] ? roots[0] : roots[1];	

		// From: Page 24

		// Compute intersection point
		pt3_scalar_op(r.intersection,r.D,*,root);
		pt3_vector_op(r.intersection,r.intersection,+,r.P);
		// Compute normal vector
		pt3_vector_op(r.normal,r.intersection,-,this->c);
		pt3_scalar_op(r.normal,r.normal,/,this->r);
		
	}

	return solutions == 2;

}


AABB Sphere::getAABB() {
    pt3 max;
    pt3 min;
    pt3_scalar_op(max,c,+,r);
    pt3_scalar_op(min,c,-,r);

    return AABB(min,max);
}


