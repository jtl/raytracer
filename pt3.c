
#include "pt3.h"

void normalize(pt3& v) {
	double magnitude = sqrt(pt3_dot(v,v));
	if(magnitude == 0) return;
	pt3_scalar_op(v,v,/,magnitude);
}
